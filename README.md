# Python lessons for librarians

### Welcome!

You can find all the files associated with the lessons in the [**data folder**](-/tree/master/data). 

You can find all the scripts associated with the lessons in the [**scripts folder**](-/tree/master/scripts). 

If you are looking for the lessons, head over to the [**wiki**](https://gitlab.com/mjanowiecki/python-lessons-for-librarians/-/wikis/home)!