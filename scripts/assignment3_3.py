import csv

# Creates new spreadsheet called 'many_fun_animals.csv'.
f = csv.writer(open('many_fun_animals.csv', 'w', newline=''))

filename = 'animals_edited.csv'
with open(filename) as csvfile:  # Opens CSV
    animalreader = csv.reader(csvfile)  # Reads CSV
    for item in animalreader:  # Loops through each row as list
        animal = item[0]  # Assigns first item in list to variable animal.
        anm_type = item[1]  # Assigns first item in list to variable anm_type.
        georange = item[2]  # Assigns third item in list to variable georange.
        lifespan = item[3]  # Assigns fourth item in list to variable lifespan.
        # Writes new row with variables to 'many_fun_animals.csv'.
        f.writerow([animal, anm_type, georange, lifespan])

# Creates lists for new animals.
zebra = ['Zebra', 'Mammal', 'Africa', '20-30 years']
woodpecker = 'Ladder-Backed Woodpecker', 'Bird', 'North America', '5 years'
wolverine = ['American Wolverine', 'Mammal', 'North America', '5-13 years']

# Writes new rows to 'many_fun_animals.csv'.
f.writerow(zebra)
f.writerow(woodpecker)
f.writerow(wolverine)
