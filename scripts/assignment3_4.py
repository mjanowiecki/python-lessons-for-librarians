import csv

# Creates new spreadsheet called 'driving_animals.csv'.
f = csv.writer(open('driving_animals.csv', 'w', newline=''))

filename = 'animals_edited.csv'
with open(filename) as csvfile:  # Opens CSV.
    animalreader = csv.reader(csvfile)  # Reads CSV.
    # Loops through each row as list.
    for count, item in enumerate(animalreader):
        print(count)
        animal = item[0]  # Assigns first item in list to variable animal.
        anm_type = item[1]  # Assigns first item in list to variable anm_type.
        georange = item[2]  # Assigns third item in list to variable georange.
        lifespan = item[3]  # Assigns fourth item in list to variable lifespan.
        lifespan = lifespan.replace(' years', '')  # Remove 'years'.
        lifespans = lifespan.split('-')  # Turn lifespans into list.
        if count > 0:  # This line helps us skip the header (first) row.
            print(lifespans)
            if len(lifespans) > 1:  # If there is more than 1 item in list.
                max_span = lifespans[1]  # Assign 2nd item in list to max_span.
                max_span = int(max_span)  # Convert max_span into an integer.
            else:
                max_span = lifespans[0]  # Assign 1st item in list to max_span.
                max_span = int(max_span)  # Convert max_span into an integer.
            # If max_span is greater or equal to 16, write row to new csv.
            if max_span >= 16:
                f.writerow([animal, anm_type, georange, lifespan])
            # Else, do nothing.
            else:
                pass
