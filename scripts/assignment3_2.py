import csv

all_types = []

filename = 'animals_edited.csv'
with open(filename) as csvfile:  # Opens CSV
    animalreader = csv.reader(csvfile)  # Reads CSV
    for item in animalreader:  # Loops through each row as list.
        anm_type = item[1]  # Assigns second item in list to variable anm_type.
        if anm_type not in all_types:
            all_types.append(anm_type)

print(all_types)

mammal = csv.writer(open('mammals.csv', 'w', newline=''))
bird = csv.writer(open('birds.csv', 'w', newline=''))
amph = csv.writer(open('amphibians.csv', 'w', newline=''))

with open(filename) as csvfile:  # Opens CSV
    animalreader = csv.reader(csvfile)  # Reads CSV
    for item in animalreader:  # Loops through each row as list.
        animal = item[0]  # Assigns first item in list to variable animal.
        anm_type = item[1]  # Assigns second item in list to variable anm_type.
        georange = item[2]  # Assigns third item in list to variable georange.
        lifespan = item[3]  # Assigns fourth item in list to variable lifespan.
        if anm_type == 'Mammal':
            mammal.writerow([animal, anm_type, georange, lifespan])
        elif anm_type == 'Bird':
            bird.writerow([animal, anm_type, georange, lifespan])
        else:
            amph.writerow([animal, anm_type, georange, lifespan])
