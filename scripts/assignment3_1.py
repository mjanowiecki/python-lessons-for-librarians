import csv

# Creates new spreadsheet called 'animals_edited.csv'
f = csv.writer(open('animals_edited.csv', 'w', newline=''))

filename = 'fun_animals.csv'
with open(filename) as csvfile:  # Opens CSV
    animalreader = csv.reader(csvfile)  # Reads CSV
    for item in animalreader:  # Loops through each row as list.
        animal = item[0]  # Assigns first item in list to variable animal.
        anm_type = item[1]  # Assigns second item in list to variable anm_type.
        georange = item[2]  # Assigns third item in list to variable georange.
        lifespan = item[3]  # Assigns fourth item in list to variable lifespan.
        animal = animal.title()  # Convert animal name to titlecase.
        anm_type = anm_type.title()  # Convert anm_type name to titlecase.
        # Removes the plurals from anm_type if there.
        if anm_type[-1] == 's':
            anm_type = anm_type[:-1]
        else:
            anm_type = anm_type
        georange = georange.title()
        lifespan = lifespan.replace('yrs', ' years')  # Normalizes years.
        # Appends 'years' to lifespan if not there.
        if "years" not in lifespan:
            lifespan = lifespan + ' years'
        else:
            lifespan = lifespan
        # Writes new row with variables to animals_edited.csv.
        f.writerow([animal, anm_type, georange, lifespan])
